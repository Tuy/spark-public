package tme2;
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class Tme2bis {

  public static class TokenizerMapper extends Mapper<Object, Text, MyKey, Text>{
	int cpt = 0;
	public static final char[] ponctuation={',','?','!','.',':',';','\'',' ', '\t','\n', '\r', '\f'};
    private Text word = new Text();
    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
      StringTokenizer itr = new StringTokenizer(value.toString(), String.copyValueOf(ponctuation));
      while (itr.hasMoreTokens()) {
        word.set(itr.nextToken());
        MyKey k = new MyKey();
        k.setMapNumber(context.getJobID().getId());
        k.setOrder(cpt);
        cpt++;
        context.write(k, word);
      }
    }
  }
  
  public static class IntSumReducer extends Reducer<MyKey,Text, IntWritable, Object> {
    private IntWritable result = new IntWritable();
    public void reduce(MyKey key, Iterable<Text> value, Context context) throws IOException, InterruptedException {
      for(Text v : value){
    	  int sum = v.toString().length();
          result.set(sum);
          context.write(result, null);
      }
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    conf.setBoolean("mapreduce.map.speculative", false);
	conf.setBoolean("mapreduce.reduce.speculative", false);
    String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
    if (otherArgs.length != 4) {
      System.err.println("Usage: poem <in1> <out1> <in2> <out2>");
      System.exit(2);
    }
    Job job1 = Job.getInstance(conf, "poem1");
    Job job2 = Job.getInstance(conf, "poem2");
    job1.setJarByClass(Tme2.class);
    job2.setJarByClass(Tme2.class);
    job1.setMapperClass(TokenizerMapper.class);
    job2.setMapperClass(TokenizerMapper.class);
    job1.setReducerClass(IntSumReducer.class);
    job2.setReducerClass(IntSumReducer.class);
    job1.setOutputKeyClass(MyKey.class);
    job2.setOutputKeyClass(MyKey.class);
    job1.setOutputValueClass(Text.class);
    job2.setOutputValueClass(Text.class);
    job1.setNumReduceTasks(1);//Il est bien sur possible de changer cette valeur (1 par défaut)
    job2.setNumReduceTasks(1);
    FileInputFormat.addInputPath(job1, new Path(otherArgs[0]));
    FileInputFormat.addInputPath(job2, new Path(otherArgs[2]));
    final Path outDir1 = new Path(otherArgs[1]);
    final Path outDir2 = new Path(otherArgs[3]);
    FileOutputFormat.setOutputPath(job1, outDir1);
    FileOutputFormat.setOutputPath(job2, outDir2);
    final FileSystem fs = FileSystem.get(conf);//récupération d'une référence sur le système de fichier HDFS
	 if (fs.exists(outDir1)) {
		 fs.delete(outDir1, true);
	 }
	 if (fs.exists(outDir2)) {
		 fs.delete(outDir2, true);
	 }
   
    System.exit(job1.waitForCompletion(true) && job2.waitForCompletion(true) ? 0 : 1);
  }
}
