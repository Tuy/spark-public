package tme3;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BooleanWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.SequenceFile.CompressionType;
import org.apache.hadoop.io.SequenceFile.Writer;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.Mapper.Context;
import org.apache.hadoop.mapreduce.lib.input.*;
import org.apache.hadoop.mapreduce.lib.output.*;
import org.apache.hadoop.util.GenericOptionsParser;

import tme2.MyKey;


public class MyPiEstimator {

	public static class Tme3Mapper extends Mapper<IntWritable, IntWritable, IntWritable, IntWritable>{
		public void map(IntWritable graine, IntWritable samples, Context context) throws IOException, InterruptedException {
			HaltonSequence hs = new HaltonSequence(graine.get());
			for (int i=0; i<samples.get(); i++){
				double[] point = hs.nextPoint();
				if (Math.sqrt(point[0]*point[0]+point[1]*point[1]) < 1){
					context.write(new IntWritable(1), new IntWritable(1));
				} else {
					context.write(new IntWritable(0), new IntWritable(1));
				}
			}
		}
	}

	public static class Tm3Reducer extends Reducer<IntWritable,IntWritable, IntWritable, Object> {
		private IntWritable result = new IntWritable();
		public void reduce(IntWritable key, Iterable<IntWritable> value, Context context) throws IOException, InterruptedException {
			int sum = 0;
			//if (key.get() == 1) {
				for(IntWritable v : value){
					sum = sum + v.get();
				}
				result.set(sum);
				context.write(result, null);
			//}
		}
	}


	public static void main(String[] args) throws Exception {
		Configuration conf = new Configuration();
		conf.setBoolean("mapreduce.map.speculative", false);
		conf.setBoolean("mapreduce.reduce.speculative", false);

		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		if (otherArgs.length != 3) {
			System.err.println("Usage: Pi <nMaps> <nReduce> <nSample>");
			System.exit(2);
		}

		//parsage des parametres
		final int numMap = Integer.parseInt(otherArgs[0]);
		final int numReduce = Integer.parseInt(otherArgs[1]);
		final long numSamples = Long.parseLong(otherArgs[2]);

		//Configuration du Job
		Job job = Job.getInstance(conf, "Pi_calulator");
		job.setInputFormatClass(SequenceFileInputFormat.class);

		job.setMapOutputKeyClass(IntWritable.class);//type cle de sortie des maps
		job.setMapOutputValueClass(IntWritable.class);//type valeur de sortie des maps


		job.setOutputKeyClass(IntWritable.class);//type cle de sortie du MapReduce
		job.setOutputValueClass(IntWritable.class);//type valeur de sortie du MapReduce


		//job.setOutputFormatClass(/*A DEFINIR*/);

		job.setMapperClass(Tme3Mapper.class);
		// ATTENTION : Ne pas utiliser cette methode pour eviter les pb de compatibilite avec les types de cle/valeur
		//job.setCombinerClass(MyPiReducer.class); 
		job.setReducerClass(Tm3Reducer.class);
		job.setNumReduceTasks(numReduce);
		job.setJarByClass(MyPiEstimator.class); 

		Path TMP_DIR = new Path("/TMP_PI");
		final Path inDir = new Path(TMP_DIR, "in");// chemin vers le repertoire d'entree
		final Path outDir = new Path(TMP_DIR, "out");// chemin vers le repertoire de sortie
		FileInputFormat.addInputPath(job,inDir);//déclaration du répertoire d'entrée
		FileOutputFormat.setOutputPath(job, outDir);//déclaration du répertoire de sortie
		// Recuperation d'une reference sur le systeme de fichier
		final FileSystem fs = FileSystem.get(conf);//récupération d'une référence sur le système de fichier
		if (fs.exists(TMP_DIR)) {
			fs.delete(TMP_DIR, true);
		}
		if (!fs.mkdirs(inDir)) {
			throw new IOException("Cannot create input directory " + inDir);
		}

		//*****************************************************************
		//*****  TODO : Definir les fichiers d'entree pour chaque map ****
		//*****************************************************************
		for (int i=1; i <= numMap; i++){
			Path pt = new Path(inDir, "in"+i);
			SequenceFile.Writer writer = SequenceFile.createWriter(conf, Writer.file(pt), Writer.keyClass(IntWritable.class), Writer.valueClass(IntWritable.class),Writer.compression(CompressionType.NONE));
			writer.append(new IntWritable(i), new IntWritable((int)numSamples));
			writer.close();
		}
		//aide: Si le input format est le SequenceFileInputFormat, 
		// Pour ecrire dans des fichiers avec le format SequenceFile utiliser l'objet SequenceFile.writer
		// pour initialiser cet objet ecrire le code
		// SequenceFile.Writer writer=
		//		SequenceFile.Writer writer = SequenceFile.createWriter( conf, Writer.file(<Path du fichier>), Writer.keyClass(<classe de la cle>), Writer.valueClass(<classe de la valeur>),Writer.compression(CompressionType.NONE));
		// ecrire dans le fichier avec writer.append(<cle>,<valeur>)


		//soumission et lancement du job sur la plateforme
		job.waitForCompletion(true);
		System.exit(0);

		//****************************************************************
		//*****  TODO : Recuperer et parser les fichiers de sorties ******
		//****************************************************************

		//****************************************************************************
		//*****           TODO : Calculer et Afficher la valeur de Pi           ******
		//****************************************************************************
	}	

}
