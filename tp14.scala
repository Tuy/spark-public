import org.apache.spark._
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD

// Q1: On créé le graph
val edges = sc.textFile("/user/etu10/facebook_edges_prop.csv", 4)
val users = sc.textFile("/user/etu10/facebook_users_prop.csv", 4)

val u1 = users.map(x=>x.split(",")).map(x=>(x(0).toLong, (x(1), x(2), x(3))))
val e1 = edges.map(x=>x.split(",")).map(x=>(Edge(x(0).toLong, x(1).toLong, (x(2), x(3)))))

val defaultUser = ("Anna", "Dao", "22")

val graph = Graph(u1, e1, defaultUser)

// Q2: On trouve Kendall
var k = graph.vertices.filter{case(id,(prenom,nom,age))=> prenom == "Kendall"}

// Q3: On trouve ceux qui ont Kendall en collègue
var kc = graph.triplets.filter(x=>(x.dstAttr._1 == "Kendall" && x.attr._2.toInt > 70 && x.attr._1 == "colleague"))
kc.collect.foreach(println(_))

// Q4:
val e4 = e1.filter{case Edge(src, dst, prop)=> prop._2.toInt > 80 && prop._1 == "friend"}
val e42 = e4.map{case Edge(src, dst, prop)=>(src, 1)}
var e43 = e42.reduceByKey((a,b)=> a+b)
e43.collect.maxBy(_._2)

// Q5 (i):
var e51 = graph.triplets.filter(x=>(x.srcAttr._1 == "Kendall"))
var e51plus = graph.triplets.filter(x=>(x.dstAttr._1 == "Kendall"))
var e512 = e51.map(x=>(x.dstAttr))
var e512plus = e51plus.map(x=>(x.srcAttr))
var e51final = e512.++(e512plus)
e51final.count
e51final.collect.foreach(println(_))

// Q5 (ii): (Kendall est 2058)
var e52 = graph.collectNeighbors(EdgeDirection.Either)
var e522 =  e52.filter(x=>(x._1 == 2058))
var e523 = e522.collect()
e523(0)._2.size

// Q5 (iii):
// Correction: graph.aggregateMessages[(Long, Array[(String, String, String)])](t => { if(t.srcAttr._1 == "Kendall"){ t.sendToSrc(t.srcId, Array(t.dstAttr)); }; if(t.dstAttr._1 == "Kendall"){ t.sendToDst(t.srcId, Array(t.srcAttr)); }}, (a,b) => (a._1, a._2 ++ b._2)).collect()(0)._2._2.foreach(println)
//var e53 = graph.aggregateMessages[(Int, Array[String, String, String])](
//    tr => {
//    	if(tr.dstAttr._1 == "Kendall"){
//		tr.sendToDst(1, Array(tr.srcAttr._1))
//	},
//	(a, b) => a._1 + b._1, a._2 ++ b._2)
//})

// Q6:
var e6 = e1.map{case Edge(src, dst, prop)=>(src, 1)}
var e61 = e6.reduceByKey((a,b)=> a+b)
// Affiche le dernier: e61.collect.minBy(_._2)
var e62 = e61.collect.sortBy(_._2)
var e63 = e61.filter(x=>x._2 == e62.take(1)(0)._2)
// Il faudrait afficher les noms

// Q7:
var e7 = e1.map{case Edge(src, dst, prop)=>(dst, 1)}
var u7 = u1.map(x=>(x._1, x._2._2))
var t7 = u7.leftOuterJoin(e7)
var t72 = t7.filter(x=>x._2._2.isEmpty)
t72.collect.foreach(println)

// Q8:
var e8src = e1.map{case Edge(src, dst, prop)=>(src, 1)}
var e8dst = e1.map{case Edge(src, dst, prop)=>(dst, 1)}
var e8src2 = e8src.reduceByKey((a,b)=>a+b)
var e8dst2 = e8dst.reduceByKey((a,b)=>a+b)
var w8 = e8src2.join(e8dst2).filter(x=>x._2._1 == x._2._2)
w8.count

// Q9:
// Etape 1: pour un seul
var e9 = graph.triplets.filter(x=>(x.dstAttr._1 == "Kendall" && x.attr._1 == "friend"))
var e92 = e9.map(x=>(x.srcAttr._2,x.srcAttr._3.toInt))
var e93 = e92.sortBy(_._2, false) 

// Etape 2: pour les trois
var e9zold = graph.triplets.filter(x=>((x.dstAttr._1 == "Kendall" || x.dstAttr._1 == "Dalvin" || x.dstAttr._1 == "Elena") && x.attr._1 == "friend"))
var e9z = graph.triplets.filter(x=>((x.dstAttr._1 == "Kendall" || x.dstAttr._1 == "Dalvin" || x.dstAttr._1 == "Elena"))
var e9z2 = e9z.map(x=>(x.dstAttr._1, (x.srcAttr._1,x.srcAttr._3.toInt)))
var e9z3 = e9z2.groupByKey()
var e9z4 = e9z3.map(x=>(x._1,x._2.toList.sortBy(y=>y._2).reverse.head))
// Il faudrait afficher le message

// Q10:
var e10 = graph.triplets.filter(x=>((x.dstAttr._1 == "Kendall" || x.dstAttr._1 == "Lilia") && x.attr._1 == "friend"))
var e102 = e10.map(x=>((x.srcAttr._2, x.srcAttr._1), 1))
var e103 = e102.reduceByKey((a,b)=>a+b).filter(x=>x._2 == 2)

// Q11:
var ag11 = graph.aggregateMessages[(Int, Int)](
	t => { // Map Function
	     if (t.attr._1 == "friend"){
	     	t.sendToSrc(1, t.dstAttr._3.toInt)
		t.sendToDst(1, t.srcAttr._3.toInt)
	     }
	},

	(a,b) => (a._1 + b._1, a._2 + b._2)
).mapValues(x=>x._2/x._1).collect.foreach(println(_))

// Q12:
var ag12 = graph.aggregateMessages[(Set[String])](
    t => {
      t.sendToSrc(Set(t.attr._1))
    },

    (a, b) => (a ++ b)
)
ag12.collect.foreach(println(_))

// => ne marche pas ==> plus d'arguments pour le outerjoin
var g12 = Graph(u1.map(x=>(x._1, x._2._1)), e1, "Olivier").outerJoinVertices(ag12)
