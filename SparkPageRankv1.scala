/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import org.apache.spark.SparkContext._
import org.apache.spark.{SparkConf, SparkContext, HashPartitioner}
import scala.math.abs
import scala.util.control.Breaks._

/**
 * Computes the PageRank of URLs from an input file. Input file should
 * be in format of:
 * URL         neighbor URL
 * URL         neighbor URL
 * URL         neighbor URL
 * ...
 * where URL and their neighbors are separated by space(s).
 */
object SparkPageRank {
  def main(args: Array[String]) {
    val sparkConf = new SparkConf().setAppName("PageRank 4")
    var iters = args(1).toInt
    val ctx = new SparkContext(sparkConf)
    val lines = ctx.textFile(args(0), 4)
    val users = ctx.textFile(args(2), 4)
    // Le map sépare les virgules
    // Distinct retire les doublons (mais à quoi sert-il?)
    // On groupe par user
    val links = lines.map{ s =>
      val parts = s.split(",")
      (parts(0), parts(1))
    }.distinct().groupByKey().partitionBy(new HashPartitioner(4)).persist().cache()
    // Comme dans ce programme on a pas de map => le partitionnement effectué par groupByKey est gardé:
    // On met ranks avec (user, 1.0)
    var ranks = links.mapValues(v => 1.0)
    val grandn = ranks.count()

    breakable { for (i <- 1 to iters) {
      // On join les ranks
      // pour chaque voisin, on attribu un rank
      val contribs = links.join(ranks).values.flatMap{ case (urls, rank) =>
        val size = urls.size
        urls.map(url => (url, rank / size))
      }
      // On additionne pour un user les ranks
      var ranksold = ranks
      ranks = contribs.reduceByKey(_ + _).mapValues(0.15 + 0.85 * _)
      var res = ranksold.join(ranks).map(x=>(1,Math.abs(x._2._2-x._2._1))).reduceByKey(_+_).map(x=>(x._2/grandn))
      var res2 = res.filter(x=>x > 0.01)
      if (res2.count() == 0){
      	 println("BREAKING")
	 break
      }
    } }

    val users2 = users.map{ x =>
    	val parts = x.split(",")
	(parts(0), (parts(1), parts(2)))		
    }
    // users2 : (id, (nom, age))
    // join : (id, ((nom, age), rank))
    // map : (rank, (nom, age, id))
    val output = users2.join(ranks).map(x=>(x._2._2, (x._2._1._1, x._2._1._2, x._1))).top(5)
    output.foreach(tup => println(tup._2._1 + " has rank: " + tup._1 + " Id " + tup._2._3 + " & Age: " + tup._2._2 + "."))

    ctx.stop()
  }
}
