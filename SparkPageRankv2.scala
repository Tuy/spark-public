/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import org.apache.spark.SparkContext._
import org.apache.spark.{SparkConf, SparkContext, HashPartitioner}

/**
 * Computes the PageRank of URLs from an input file. Input file should
 * be in format of:
 * URL         neighbor URL
 * URL         neighbor URL
 * URL         neighbor URL
 * ...
 * where URL and their neighbors are separated by space(s).
 */
object SparkPageRank {
  def main(args: Array[String]) {
    // On créé une configuration et on donne un nom à l'application
    val sparkConf = new SparkConf().setAppName("PageRank V2")
    // On charge le deuxième argument (nombre d'itérations)
    var iters = args(1).toInt
    // On charge la configuration dans le contexte
    val ctx = new SparkContext(sparkConf)
    // On charge le fichier donné en argument
    val lines = ctx.textFile(args(0), 4)
    // Le premier map: (user, voisin) en splitant la virgule
    // On groupe les voisins par user
    // Le deuxième map: (user, (<voisins>, nombre de voisin))
    // Ensuite on met en cache
    val links = lines.map{ s =>
      val parts = s.split(",")
      (parts(0), parts(1))
    }.groupByKey().map(url=>(url._1, (url._2, url._2.size))).cache().partitionBy(new HashPartitioner(4)).persist()

    // Le groupe by key est inutile ici
    // Le map retourne (user, 1.0)
    var ranks = links.groupByKey().map(url => (url._1,1.0))

    // On fait autant d'itération que demandé en argument
    for (i <- 1 to iters) {
      // le join retourne (user, ((<voisins>, nombre de voisin), 1))
      // le flatmap retorune (user, rang)
      val contribs = links.join(ranks).flatMap{ case (url, ((voisins,size), rank)) =>
      	  voisins.map(url => (url, rank / size))
      }
      // Le reduce by key sert à faire une somme pour les voisins
      // le map values refait un calcul sur le rang
      ranks = contribs.reduceByKey(_ + _).mapValues(0.15 + 0.85 * _)
    }

    // On récupère le résultat et on l'affiche
    val output = ranks.collect()
    output.foreach(tup => println(tup._1 + " has rank: " + tup._2 + "."))

    ctx.stop()
  }
}
