Données: /user/hduser/cooc/input/oc_2010b.csv
=> (doc, tid)

Terms2 (/user/hduser/cooc/terms2)
=> (tid, term)

Exo1:
Sujet -> Obtenir la co-occurence des couples de termes : c(t1, t2, n)
      -> t1 et t2 sont des tids

val oc = sc.textFile("/user/hduser/cooc/input/oc_2010b.csv", 4)
var ocsplit = oc.map(x=>(x.split(",")))   [Array[String]]
var oc2 = ocsplit.map(x=>(x(0),x(1)))    [(String, String)]
var oc3 = oc2.groupByKey()    [(String, Iterable[String])]
var oc3bis = oc2.join(oc2)    [(String, (String, String))]


=== Sur le sample ave JOIN
val oc = sc.textFile("/user/hduser/cooc/input/oc_2010b.csv", 4)
var ocsample = oc.takeSample(false, 1000)
var sample = sc.parallelize(ocsample, 4)
var sampsplit = sample.map(x=>(x.split(",")))
var s2 = sampsplit.map(x=>(x(0),x(1))) 
var s3 = s2.join(s2)
var s4 = s3.map(x=>(x._2, 1))
var s5 = s4.reduceByKey((a,b)=>(a+b)) [((String, String), Int)]
// pour voir ceux qui sont au dessus de 1 : s6.filter(x=>x._2 > 1).take(10)
var s6 = s5.filter(y=>y._1._1!=y._1._2)
// Il faudrait aussi filter les doublons ou on a (t1,t2) et (t2,t1)
// => On pourrait réordonnancer la clé dans l'ordre alphabétique et après refiltré les doublons


=== Solution avec groupByKey()
val oc = sc.textFile("/user/hduser/cooc/input/oc_2010b.csv", 4)
var ocsample = oc.takeSample(false, 1000)
var sample = sc.parallelize(ocsample, 4)
var sampsplit = sample.map(x=>(x.split(",")))
var s2 = sampsplit.map(x=>(x(0),x(1)))
var s3 = s2.groupByKey()     Array[(String, Iterable[String])]




Exo 2: RDF Distribué
triplet(s, p, o)
    [sommet, arc, sommet]
Si on veut partitionner dans spark correctement les chemins qui sont ensembles (ex: s1 =(p1)=> o1 =(p2)=>o2, + une autre branche)
Déterminer le graph le plus étendu qu'on peut atteindre à partir d'un noeud pour partitionner correctement les données.
( map partition with index, map partitions, ... ? )
=> faire ça à partir des partitions initiales de spark
[ graph avec diamètre de 5-7 noeuds minimum ] => donc on fait que jusqu'au diamètre
[ On peut le faire sur un graph à 10 noeuds, ou DBPedia ]
